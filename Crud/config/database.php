<?php
return [
    
'default' => 'sqlite',
    'connections' => [
        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => storage_path('database.sqlite'),
            'prefix'   => env('DB_PREFIX', ''),
        ],
        'accounts' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'accounts',
            'username'  => 'root',
            'password'  => 'secret',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'content' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'content',
            'username'  => 'root',
            'password'  => 'secret',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
    ],
];