var modulo = angular.module('registro', ['ui.bootstrap']);

modulo.controller("RegisController", ["$http", "$sce", enviarData]);

function enviarData($http, $sce, $scope){
    var en = this;
    en.datos = {};
    en.enviar = function(){
        $http.post("registrar", en.datos)
            .success(function(res){
                if(res.sms == 2){
                    en.sms = $sce.trustAsHtml("<div class='alert alert-danger'><i class='fa fa-info-circle'></i> Ha ocurrido un error..</div>");
                }else
                if(res.sms == 1){
                    en.sms = $sce.trustAsHtml("<div class='alert alert-success'><i class='fa fa-check-circle'></i> Se ha Almacenado correctamente..</div>");
                    en.listar();
                    en.datos.id = "";
                    en.datos.nombre = "";
                    en.datos.email = "";
                    en.datos.sexo = "";
                    en.datos.desc = "";
                }
                setTimeout(function(){
                    en.cerrar();
                },2000);
        }); 
    }
    
    en.listar  = function(){
        $http.post("listar")
            .success(function(resp){
               en.lista = $sce.trustAsHtml(resp.tabla);
        });
    }
     
    
    en.cerrar = function(){
        angular.element( document.querySelector('.alert')).remove();
    }
    
    en.actualizar = function(id, nombre, correo, sexo, desc){
        en.datos.id = id;
        en.datos.nombre = nombre;
        en.datos.email = correo;
        en.datos.sexo = sexo;
        en.datos.desc = desc;
        
    }
    
    en.eliminar = function(id){
        var data = {'id':id};
        $http.post("eliminar", data)
        .success(function(resp){
            if(resp.resultado == 1){
                en.sms = $sce.trustAsHtml("<div class='alert alert-success'><i class='fa fa-check-circle'></i> Se ha borrado correctamente..</div>");
                en.listar();
            }else{
                en.sms = $sce.trustAsHtml("<div class='alert alert-danger'><i class='fa fa-info-circle'></i> Ha ocurrido un error..</div>"); 
            }
            setTimeout(function(){
                en.cerrar();
            },2000);
        });
    } 
}

//directiva para copilar div

modulo.directive('dir', function($compile, $parse){
    return {
        link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
            function getStringValue() { return (parsed(scope) || '').toString(); }
            scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);
            });
        }
    }
})
 



