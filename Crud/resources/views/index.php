<!doctype html>
<html lang="es" ng-app="registro">
    <head>
        <meta charset="utf-8">
        <title>Index</title>
        <link rel="stylesheet" href="/archivos/css/bootstrap.css">
        <link rel="stylesheet" href="/archivos/font-awesome/css/font-awesome.css">
    </head>
    <body ng-controller="RegisController as en">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="navbar-header">
               <a title="Crud Lumen, bootstrap, angular" class="navbar-brand" href="#"><i class = "fa fa-css3" ></i> CLBA</a>
            </div>
            <div id="navbarCollapse" class="collapse navbar-collapse navbar-ex1-collapse">
            </div>  
        <div style="background-color:#ff420e;width:34%;height:5px;float:left;" ></div>  
        <div style="background-color:#ff950e;width:32%;height:5px;float:left;" ></div>
        <div style="background-color:#579d1c;width:34%;height:5px;float:left;" ></div>
        </div>
        <div style="padding:30px;"></div>
        <div class="row" style="margin: 0">
            <div class="col-sm-12">
                <div ng-bind-html="en.sms" class="sms" id='sms'>
                    
                </div>
            </div> 
        </div>
        <div style="padding:10px;"></div>
        <div class="row" style="margin: 0">
            <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <form class="form-horizontal" ng-submit="en.enviar()">
                        <fieldset>
                            <legend>Registro</legend>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" ng-model="en.datos.nombre" required class="form-control" .datos.nombreid="nombre" name='nombre' placeholder="Nombre">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="email" ng-model="en.datos.email" required class="form-control" id="email" name='email' placeholder="Correo">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select name='sexo' ng-model="en.datos.sexo" required class="form-control" id='sexo'>
                                        <option value="">[Seleccione Sexo]</option>
                                        <option>Masculino</option>
                                        <option>Femenino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea id='desc' required name='desc' ng-model="en.datos.desc" class="form-control" placeholder="Ingrese descripción"></textarea>
                                    <input type="hidden" name="id" ng-model="en.datos.id">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 right">
                                    <button type="reset" class="btn btn-default">Limpiar</button>
                                    &nbsp;
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                    &nbsp;
                                    <button type="button" ng-click="en.listar()" class="btn btn-info">Listar</button>
                                </div>
                            </div>
                        </fieldset>    
                    </form>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
        <div style="padding:30px;"></div>
        <div class="row" style="margin: 0">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <table class="table table-striped table-hover">
                    <thead>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Sexo</th>
                        <th>Descripción</th>
                        <th>Creado</th>
                        <th>Actualizado</th>
                    </thead>
                    <tbody ng-bind-html="en.lista" dir="dir">
                        
                    </tbody>
                </table>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </body>
        <script src="/archivos/js/angular.js"></script>
        <script src="/archivos/js/angular-animate.js"></script>
        <script src="/archivos/js/angular-touch.js"></script>
        <script src="/archivos/js/angular-sanitize.js"></script>
        <script src="/archivos/js/ui-bootstrap-tpls-1.2.4.js"></script>
        <script src="/archivos/js/index.js"></script>
</html>