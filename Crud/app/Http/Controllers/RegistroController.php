<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registro;

class RegistroController extends Controller
{
   public function registrar(Request $request){
        $nombre =  $request->input("nombre");
        $email = $request->input("email");
        $desc =  $request->input("desc");
        $sexo = $request->input("sexo");
        $id = $request->input("id");
        $resp = array();
        if($id == "" || $id == 0){
            $registro = new Registro();
        }else{
            $registro = Registro::Find($id);
        }
        $registro->nombre = $nombre;
        $registro->correo = $email;
        $registro->desc = $desc;
        $registro->sexo = $sexo;
        $resp['sms'] = ($registro->save())?1:2;
        return response()->json($resp);
   }
   
   public function listar(){
       $registro = Registro::all();
       $res = array();
       $table = "";
       
       foreach ($registro as $datos){
        $table .= "<tr>";
            $table .= "<td>".$datos->id."</td>";
            $table .= "<td>".$datos->nombre."</td>";
            $table .= "<td>".$datos->correo."</td>";
            $table .= "<td>".$datos->sexo."</td>";
            $table .= "<td>".$datos->desc."</td>";
            $table .= "<td>".$datos->created_at."</td>";
            $table .= "<td>".$datos->updated_at."</td>";
            $table .= '<td><button ng-click="en.actualizar('.$datos->id.',\''.$datos->nombre.'\',\''.$datos->correo.'\',\''.$datos->sexo.'\',\''.$datos->desc.'\')"><i class="fa fa-check"  style="color:#449D44; cursor:pointer;"></i></button></td>';
            $table .= "<td><button ng-click='en.eliminar(".$datos->id.")'><i class='fa fa-close' style='color:#f23315; cursor:pointer;'></i></button></td>";
        $table .= "</tr>";
       }
       
       $res['tabla']=$table;
       
       return response()->json($res);
    }
    
    public function eliminar(Request $request){
        $id = $request->input("id");
        $resgitro = Registro::Find($id);
        $resp = array();
        $resp['resultado'] = ($resgitro->delete())? 1 : 0;
        return response()->json($resp);
    }
}